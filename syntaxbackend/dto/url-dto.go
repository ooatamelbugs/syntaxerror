package dto

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type UrlDTO struct {
	Url    string             `json:"url" form:"url" binding:"required" validate:"min=3"`
	Secret string             `json:"secret" form:"secret" binding:"required" validate:"min=3"`
	UserID primitive.ObjectID `json:"user_id" form:"user_id" binding:"required" validate:"min=3"`
	Expire time.Time          `json:"expire" form:"expire" binding:"required" validate:"min=3"`
	Status bool               `json:"status" form:"status" default:"false" validate:""`
}
