package dto

type EmailDTO struct {
	To      string
	Subject string
	Message interface{}
}
