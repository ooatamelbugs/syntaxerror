package dto

import "go.mongodb.org/mongo-driver/bson/primitive"

type UserDTO struct {
	FirstName string `json:"firstname" form:"firstname" binding:"required" validate:"min=3"`
	LastName  string `json:"lastname" form:"lastname" binding:"required" validate:"min=3"`
	Password  string `json:"password" form:"password" binding:"required" validate:"min=8"`
	Username  string `json:"username" form:"username" binding:"required" validate:"required,email"`
}

type UpdateUserDTO struct {
	ID        primitive.ObjectID `json:"id" form:"id" binding:"required"`
	FirstName string             `json:"firstname" form:"firstname" binding:"required" validate:"min:3"`
	LastName  string             `json:"lastname" form:"lastname" binding:"required" validate:"min:3"`
	Password  string             `json:"password" form:"password" binding:"required" validate:"min:8"`
	Username  string             `json:"username" form:"username" binding:"required" validate:"email,min:3"`
}

type LoginDTO struct {
	Password string `json:"password" form:"password" binding:"required" validate:"min=8"`
	Username string `json:"username" form:"username" binding:"required" validate:"email,min=3"`
}
