package helper

import "strings"

// make struct with shape of data that will to Response
type Response struct {
	Status  bool                   `json:"status"`
	Message string                 `json:"message"`
	Data    map[string]interface{} `json:"data"`
	Errors  interface{}            `json:"errors"`
}

// function for handel return response
// @params { status of response as bool }
// @params { message of response as string }
// @params { data of response as interface }
// @params { err of response as interface }
// return response
func ResponseRestuen(status bool, message string, data map[string]interface{}, err string) Response {
	// make errorMessage variable to carry error message in it
	var errorMessage interface{}
	// sure if is  variable err that pass is not empty
	if err != "" {
		// if not empty spit the err sting and put it in  errorMessage
		errorMessage = strings.Split(err, "\n")
	}

	// make variable response that from type  Response and pass it all params
	response := Response{
		Status:  status,
		Message: message,
		Data:    data,
		Errors:  errorMessage,
	}
	// return the response object
	return response
}
