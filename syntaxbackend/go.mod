module gitlab.com/ooatamelbugs/syntaxerror

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.7.7
	github.com/go-playground/validator/v10 v10.10.1
	github.com/joho/godotenv v1.4.0
	github.com/mashingan/smapping v0.1.13
	go.mongodb.org/mongo-driver v1.8.4
	golang.org/x/crypto v0.0.0-20211215153901-e495a2d5b3d3
)
