package routes

import (
	"context"
	"log"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/ooatamelbugs/syntaxerror/config"
	"gitlab.com/ooatamelbugs/syntaxerror/controller"
	"gitlab.com/ooatamelbugs/syntaxerror/repository"
	"gitlab.com/ooatamelbugs/syntaxerror/services"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// constant for globle and init the the repository of user and Service and Controller
// and Collection
var (
	userCollection *mongo.Collection         = config.Getcollection(config.DB, "users")
	userRepository repository.UserRepository = repository.NewUserRepository(userCollection)
	userService    services.UserService      = services.NewUserService(userRepository)
	userController controller.UserController = controller.NewUserController(userService, urlService)
)

func CreateIndexUser() {
	var index = mongo.IndexModel{
		Keys: bson.M{
			"username": 1,
		},
		Options: options.Index().SetUnique(true),
	}
	var opt = options.CreateIndexes().SetMaxTime(10 * time.Second)
	var _, err = userCollection.Indexes().CreateOne(context.Background(), index, opt)
	if err != nil {
		log.Fatal(err)
	}
}

// make all routes for user here
// take server engin as param
func UserRoutes(router *gin.Engine) {
	// route for get data from / path
	router.GET("/", userController.Getindex)
	// route for create data change the state by post /api/user
	router.POST("/api/user/", userController.CreateUser)
	// route for update data change the state by post /api/user/:userId
	router.PATCH("/api/user/:id", userController.UpdateUser)
}
