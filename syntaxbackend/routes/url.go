package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/ooatamelbugs/syntaxerror/config"
	"gitlab.com/ooatamelbugs/syntaxerror/controller"
	"gitlab.com/ooatamelbugs/syntaxerror/repository"
	"gitlab.com/ooatamelbugs/syntaxerror/services"
	"go.mongodb.org/mongo-driver/mongo"
)

var (
	urlCollection *mongo.Collection         = config.Getcollection(config.ConnectdDB(), "url")
	urlRepository repository.UrlRrepository = repository.NewUrlRrepository(urlCollection)
	urlService    services.UrlService       = services.NewUrlService(urlRepository)
	urlController controller.UrlController  = controller.NewUrlController(urlService, userService, jwtService, userAuth)
)

func UrlRoutes(route *gin.Engine) {
	route.POST("/active/:secret", urlController.UpdateURL)
}
