package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/ooatamelbugs/syntaxerror/controller"
	"gitlab.com/ooatamelbugs/syntaxerror/middleware"
	"gitlab.com/ooatamelbugs/syntaxerror/services"
	"gitlab.com/ooatamelbugs/syntaxerror/utils/service"
)

// constant for globle and init the the repository of user and Service and Controller
var (
	userAuth       services.AuthService      = services.NewAuthService(userRepository)
	jwtService     service.JWTService        = service.NewjJWTService()
	authController controller.AuthController = controller.NewAuthController(userAuth, jwtService, userService)
)

// make all routes for auth user here
// take server engin as param
func AuthRoutes(router *gin.Engine) {
	// route for auth login user
	router.POST("api/auth/login", authController.Login)
	// route for auth logout
	router.PATCH("api/auth/logout", middleware.AutherizeJWT(jwtService), authController.LogOut)
}
