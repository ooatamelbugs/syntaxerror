package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type URL struct {
	ID     primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	UserID primitive.ObjectID `json:"user_id,omitempty" bson:"user_id,omitempty"`
	Url    string             `json:"url" bson:"url" validate:"required"`
	Secret string             `json:"secret" bson:"secret" validate:"required"`
	Expire time.Time          `json:"expire" bson:"expire" validate:"required"`
	Status bool               `json:"status" bson:"status" default:"false"`
}
