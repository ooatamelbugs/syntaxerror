package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Solves struct {
	ID     primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	UserID primitive.ObjectID `json:"user_id,omitempty" bson:"user_id,omitempty"`
	Solve  string             `json:"solve" bson:"solve" validate:"required"`
	Vote   int64              `json:"vote" bson:"vote" default:"0"`
}

type Problem struct {
	ID             primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	UserID         primitive.ObjectID `json:"user_id,omitempty" bson:"user_id,omitempty"`
	Question       string             `json:"question" bson:"question" validate:"required"`
	Description    string             `json:"description" bson:"description" validate:""`
	PresentSolves  []Solves           `json:"present_solves" bson:"present_solves"`
	Vote           int64              `json:"vote" bson:"vote" default:"0"`
	RequestToSolve int64              `json:"request_to_solve" bson:"request_to_solve" default:"1"`
	CreatedAt      time.Time          `json:"created_at" bson:"created_at" validate:""`
	Tags           []string           `json:"tags" bson:"tags" validate:""`
}
