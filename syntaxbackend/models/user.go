package models

import "go.mongodb.org/mongo-driver/bson/primitive"

// descirbe of data in users.Location collection
type location struct {
	Country string `json:"country"`
	City    string `json:"city"`
}

// descirbe of data in users collection
type User struct {
	ID        primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	FirstName string             `json:"firstname,omitempty" bson:"first_name" validate:"required"`
	LastName  string             `json:"lastname,omitempty" bson:"last_name" validate:"required"`
	Username  string             `json:"username,omitempty" bson:"username" validate:"required"`
	Password  string             `json:"password" bson:"password" validate:"required"`
	Active    bool               `json:"active,omitempty" bson:"active" validate:""`
	Location  location           `json:"location" bson:"location"`
	Token     string             `json:"token" bson:"token" default:""`
}
