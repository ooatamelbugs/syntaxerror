package utils

import (
	"bytes"
	"encoding/json"
	"net/http"
)

func Sendrequest(params ...string) (interface{}, error) {
	var values = map[string]string{"username": params[1], "password": params[2]}
	var data, _ = json.Marshal(values)
	response, err := http.Post(params[0], "application/json", bytes.NewBuffer(data))
	return response, err
}
