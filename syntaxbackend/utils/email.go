package utils

import (
	"fmt"
	"log"
	"net/smtp"
	"os"

	"gitlab.com/ooatamelbugs/syntaxerror/dto"
)

// function for sending a email for many puropse
// it take email dto
// and return bool or error
func SendEmail(templete dto.EmailDTO) (bool, error) {
	// email that will send from it
	var from = os.Getenv("MAIL_FROM")
	// email username AND password
	var username = os.Getenv("MAIL_USERNAME")
	var pass = os.Getenv("MAIL_PASSWORD")
	// send for some one
	var toMail = []string{templete.To}

	// determine the host and port
	var address = os.Getenv("MAIL_HOST") + ":" + os.Getenv("MAIL_PORT")
	fmt.Println(templete)
	fmt.Println(address)

	// put the Subject
	var subject = "Subject: " + templete.Subject + "\r\n"

	// determine the body and message
	var body = templete.Message.(string)
	var message = []byte(subject + body)
	// make auth with mail
	var auth = smtp.PlainAuth("", username, pass, os.Getenv("MAIL_HOST"))
	// send email
	var err = smtp.SendMail(address, auth, from, toMail, message)
	// check if error get error
	if err != nil {
		log.Fatalf("error in send mail %v", err)
		return false, err

	} else {
		return true, nil
	}
}
