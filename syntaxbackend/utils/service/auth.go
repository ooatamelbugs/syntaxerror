package service

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type JWTService interface {
	GenerateToken(userID primitive.ObjectID, username string) string
	VerifyToken(token string) (*jwt.Token, error)
}

type jwtServiceClaims struct {
	UserID   string `json:"user_id"`
	Username string `json:"username"`
	jwt.StandardClaims
}

type jwtService struct {
	secretkey string
	issuer    string
}

func NewjJWTService() JWTService {
	return &jwtService{
		issuer:    "ydhrub",
		secretkey: os.Getenv("JWTSCRETE"),
	}
}

func (jwtServ *jwtService) GenerateToken(UserID primitive.ObjectID, Username string) string {
	claims := &jwtServiceClaims{
		UserID.Hex(),
		Username,
		jwt.StandardClaims{
			ExpiresAt: time.Now().AddDate(1, 0, 0).Unix(),
			Issuer:    jwtServ.issuer,
			IssuedAt:  time.Now().Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tok, err := token.SignedString([]byte(jwtServ.secretkey))
	if err != nil {
		log.Fatalf("error to Generate Token %v\n", err)
	}
	return tok
}

func (jwtServ *jwtService) VerifyToken(token string) (*jwt.Token, error) {
	return jwt.Parse(token, func(t *jwt.Token) (interface{}, error) {
		if _, ok := t.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("token is in vailde", t.Header["alg"])
		}
		return []byte(jwtServ.secretkey), nil
	})
}
