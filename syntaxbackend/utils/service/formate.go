package service

import (
	"gitlab.com/ooatamelbugs/syntaxerror/models"
	"go.mongodb.org/mongo-driver/bson"
)

// this function for change Formate from bson.m to model type
func ServiceFormateUser(fromFormate bson.M) models.User {
	var toFormate models.User
	bsonByte, _ := bson.Marshal(fromFormate)
	bson.Unmarshal(bsonByte, &toFormate)
	return toFormate
}
