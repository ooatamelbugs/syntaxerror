package service

import (
	"encoding/base64"
	"math/rand"
	"os"
)

func generateRandom(length int) string {
	var chars = []rune("QWERTYUIOPASDFGHJKLZXCVBNM1234567890qwertyuiopasdfghjklzxcvbnm")
	var str = make([]rune, length)
	for i := range str {
		str[i] = chars[rand.Intn(len(chars))]
	}
	return string(str)
}

func UrlGenerate() string {
	var str = generateRandom(25)
	var encstr = base64.StdEncoding.EncodeToString([]byte(str))
	return os.Getenv("URL") + encstr
}
