package service

import (
	"log"

	"golang.org/x/crypto/bcrypt"
)

// function that hashed password
// return password string
func HashPassword(password []byte) string {
	// Generate hash password
	hash, err := bcrypt.GenerateFromPassword(password, bcrypt.MinCost)
	// check if err in Generate
	if err != nil {
		log.Fatalf("error in Generate hash password %v\n ", err)
	}
	// return hash as string
	return string(hash)
}

// function for verifecation password and compare it
// it return true if ok or false if not
func VerifyPassword(passwordHashed string, password []byte) bool {
	result := bcrypt.CompareHashAndPassword([]byte(passwordHashed), []byte(password))
	if result != nil {
		log.Fatal(result)
		return false
	}

	return true
}
