package services

import (
	"fmt"
	"log"

	"github.com/mashingan/smapping"
	"gitlab.com/ooatamelbugs/syntaxerror/dto"
	"gitlab.com/ooatamelbugs/syntaxerror/models"
	"gitlab.com/ooatamelbugs/syntaxerror/repository"
	"gitlab.com/ooatamelbugs/syntaxerror/utils"
	"gitlab.com/ooatamelbugs/syntaxerror/utils/service"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// this is interface of the functions will impl it
type UserService interface {
	CreateUser(user dto.UserDTO) models.User
	IndexGet() (string, error)
	GetUserByAtrribute(attrKey string, val interface{}) (models.User, error)
	Update(userID string, user dto.UpdateUserDTO) interface{}
	ConfirmMail(username string) (string, error)
}

// make userRepository is the type of userService
type userService struct {
	userRepository repository.UserRepository
}

// make function for make new UserService
// taht take UserRepository as param and retrun of interface of UserService
func NewUserService(userRepo repository.UserRepository) UserService {
	return &userService{
		userRepository: userRepo,
	}
}

// this function for test
func (userServ *userService) IndexGet() (string, error) {
	return userServ.userRepository.IndexGetString()
}

// this function for create user data service
// it take one param
// user dto type
// it return interface of user data
func (userServ *userService) CreateUser(user dto.UserDTO) models.User {
	userModel := models.User{}
	objID := primitive.NewObjectID()
	err := smapping.FillStruct(&userModel, smapping.MapFields(&user))
	if err != nil {
		log.Fatalf("error in smapping data %v\n", err)
	}
	userModel.ID = objID
	// hashed password
	hashpassword := service.HashPassword([]byte(user.Password))
	userModel.Password = hashpassword
	result, _ := userServ.userRepository.CreateUser(userModel)
	fmt.Print(result)
	return userModel
}

// this function for update user data service
// it take two param
// useriD , user dto type
// it return interface of user data
func (userServ *userService) Update(userID string, user dto.UpdateUserDTO) interface{} {
	userModel := models.User{}
	ObjID, _ := primitive.ObjectIDFromHex(userID)
	user.ID = ObjID
	err := smapping.FillStruct(&userModel, smapping.MapFields(&user))
	if err != nil {
		log.Fatalf("error in smapping data %v\n", err)
	}
	if userModel.Password != "" {
		userModel.Password = service.HashPassword([]byte(userModel.Password))
	} else {
		data, err := userServ.userRepository.GetUserByAtrribute("username", userModel.ID)
		if err != nil {
			log.Fatalf("error to get user by username %v\n ", err)
		}
		user.Password = data.Password
	}
	userData, err := userServ.userRepository.UpdateUser(userModel)

	if err != nil {
		log.Fatalf("error to get and update data %v\n", err.Error())
	}

	// result := service.ServiceFormate(userData, userModel)
	return userData
}

// function to get user by any of  Atrribute you want to pass
// it take attrKey and val as value of it
// it return data of user and error if occure
func (userServ *userService) GetUserByAtrribute(attrKey string, val interface{}) (models.User, error) {
	// check if attrKey is equal to _id
	if attrKey == "_id" {
		// convert val to string
		valstr, _ := val.(string)
		// convert val to primitive ObjectID
		objID, _ := primitive.ObjectIDFromHex(valstr)
		val = objID
	}
	// get user data or error
	data, err := userServ.userRepository.GetUserByAtrribute(attrKey, val)
	if err != nil {
		log.Fatalf("error to get user by this Atrribute %v\n ", err)
	}
	return data, err
}

func (userService *userService) ConfirmMail(username string) (string, error) {
	var mail dto.EmailDTO
	mail.To = username
	mail.Subject = "confirm mail"
	url := service.UrlGenerate()
	mail.Message = "this is the confirm link \n " + url
	_, err := utils.SendEmail(mail)
	if err != nil {
		log.Fatalf("error in send email %v ", err)
		return "", err
	}
	return url, nil
}
