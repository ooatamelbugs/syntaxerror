package services

import (
	"errors"
	"log"

	"gitlab.com/ooatamelbugs/syntaxerror/models"
	"gitlab.com/ooatamelbugs/syntaxerror/repository"
)

type UrlService interface {
	AddURL(url models.URL) models.URL
	EditURL(url models.URL) (bool, error)
	GetURL(secret string) (models.URL, error)
}

type urlService struct {
	urlRepository repository.UrlRrepository
}

func NewUrlService(urlRepo repository.UrlRrepository) UrlService {
	return &urlService{
		urlRepository: urlRepo,
	}
}

func (urlServ *urlService) AddURL(url models.URL) models.URL {
	_, err := urlServ.urlRepository.GetURLBySecret(url.Secret)
	if err == nil {
		log.Fatalf("error in get url %v", err)
	}

	_, errAddURL := urlServ.urlRepository.AddURL(url)
	if errAddURL != nil {
		log.Fatal(errAddURL)
	}

	return url
}

func (urlServ *urlService) EditURL(url models.URL) (bool, error) {
	result := urlServ.urlRepository.EditURL(url)
	if !result {
		err := errors.New("error in edit data")
		return result, err
	}
	return result, nil
}

func (urlServ *urlService) GetURL(secret string) (models.URL, error) {
	result, err := urlServ.urlRepository.GetURLBySecret(secret)
	return result, err
}
