package services

import (
	"log"

	"gitlab.com/ooatamelbugs/syntaxerror/dto"
	"gitlab.com/ooatamelbugs/syntaxerror/models"
	"gitlab.com/ooatamelbugs/syntaxerror/repository"
	"gitlab.com/ooatamelbugs/syntaxerror/utils/service"
	"go.mongodb.org/mongo-driver/bson"
)

type AuthService interface {
	Login(user dto.LoginDTO) (bson.M, bool)
	Logging(user models.User) bool
}

type authService struct {
	userRepository repository.UserRepository
}

func NewAuthService(userRepo repository.UserRepository) AuthService {
	return &authService{
		userRepository: userRepo,
	}
}

// function for Log user in
func (authService *authService) Login(user dto.LoginDTO) (bson.M, bool) {
	result, _ := authService.userRepository.VerifyUser(user.Username, user.Password)
	if result != nil {
		verifyPassword := service.VerifyPassword(result["password"].(string), []byte(user.Password))
		if result["username"].(string) == user.Username && verifyPassword {
			return result, true
		}
		return nil, false
	}

	return nil, false
}

// function for Logging user and update token
// it take user model as param
// return bool
func (authService *authService) Logging(user models.User) bool {
	// fmt.Print(user.ID)
	_, err := authService.userRepository.UpdateUser(user)
	if err != nil {
		log.Fatalf("error in update token %v\n", err)
	}
	return true
}
