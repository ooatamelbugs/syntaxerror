package config

import (
	"context"
	"fmt"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// function connection to db and create mongo client
// it retrun  instance from mongo Client
func ConnectdDB() *mongo.Client {
	// create new client and connect to db by URI
	client, err := mongo.NewClient(options.Client().ApplyURI(EnvMongoURI()))

	// check if is any error in connection
	if err != nil {
		log.Fatalf("error to make clent and connect to uri %v\n", err)
	}

	// determine the timeout to connection with db
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	// pass the time timeout connection to connect client
	err = client.Connect(ctx)

	if err != nil {
		log.Fatalf("error in connect timeout make connection %v\n ", err)
	}

	// see client is ping any error
	err = client.Ping(ctx, nil)
	if err != nil {
		log.Fatalf("ping connection to database %v\n ", err)
	}

	fmt.Println("connect to db")
	// return client object connection
	return client
}

// make instance from mongo Client
var DB *mongo.Client = ConnectdDB()

// function to get collection from db
// pass param @{client mongo client}
// and collection name that want to get it
// retrun the collection
func Getcollection(client *mongo.Client, collectionName string) *mongo.Collection {
	// get collection from syntaxerror db
	collection := client.Database("syntaxerror").Collection(collectionName)
	return collection
}
