package config

import (
	"log"
	"os"

	"github.com/joho/godotenv"
)

// function return string of mongodb uri
func EnvMongoURI() string {
	// load env from package godotenv
	err := godotenv.Load()

	// check if get error from  load func
	// get out err in log
	if err != nil {
		log.Fatalf("error to load env %v\n", err)
	}

	// return veriable MONGOURI from env
	return os.Getenv("MONGOURI")
}
