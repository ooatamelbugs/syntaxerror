package controller

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/ooatamelbugs/syntaxerror/helper"
	"gitlab.com/ooatamelbugs/syntaxerror/services"
	"gitlab.com/ooatamelbugs/syntaxerror/utils/service"
)

type UrlController interface {
	UpdateURL(ctx *gin.Context)
}

type urlController struct {
	urlService  services.UrlService
	userService services.UserService
	jwtService  service.JWTService
	authService services.AuthService
}

func NewUrlController(
	urlServ services.UrlService,
	userServ services.UserService,
	jwtServ service.JWTService,
	authServ services.AuthService,
) UrlController {
	return &urlController{
		userService: userServ,
		urlService:  urlServ,
		jwtService:  jwtServ,
		authService: authServ,
	}
}

func (urlControl *urlController) UpdateURL(ctx *gin.Context) {
	secret := ctx.Param("secret")
	if secret != "" {
		response := helper.ResponseRestuen(false, "error", nil, "invalide url")
		ctx.AbortWithStatusJSON(http.StatusConflict, response)
		return
	}

	data, err := urlControl.urlService.GetURL(secret)
	if err != nil {
		response := helper.ResponseRestuen(false, "error", nil, "invalide url")
		ctx.AbortWithStatusJSON(http.StatusConflict, response)
		return
	}

	today := time.Now()

	if compare := today.After(data.Expire); compare {
		response := helper.ResponseRestuen(false, "error", nil, "expire url")
		ctx.AbortWithStatusJSON(http.StatusConflict, response)
		return
	}
	user, err := urlControl.userService.GetUserByAtrribute("_id", data.UserID)

	if err != nil {
		response := helper.ResponseRestuen(false, "error", nil, "expire url")
		ctx.AbortWithStatusJSON(http.StatusConflict, response)
		return
	}
	generateToken := urlControl.jwtService.GenerateToken(user.ID, user.Username)
	user.Token = generateToken
	user.Active = true
	if user.Token != "" && user.Active {
		urlControl.authService.Logging(user)
		response := helper.ResponseRestuen(true, "go", map[string]interface{}{"data": user}, "")
		ctx.JSON(http.StatusOK, response)
		return

	}

	response := helper.ResponseRestuen(false, "error", nil, "error")
	ctx.AbortWithStatusJSON(http.StatusConflict, response)
}
