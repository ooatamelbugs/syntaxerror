package controller

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gitlab.com/ooatamelbugs/syntaxerror/dto"
	"gitlab.com/ooatamelbugs/syntaxerror/helper"
	"gitlab.com/ooatamelbugs/syntaxerror/services"
	"gitlab.com/ooatamelbugs/syntaxerror/utils/service"
)

type AuthController interface {
	Login(ctx *gin.Context)
	LogOut(ctx *gin.Context)
}

type authController struct {
	authServiceImpl services.AuthService
	jwtService      service.JWTService
	userServ        services.UserService
}

func NewAuthController(authServ services.AuthService, jwtServ service.JWTService, userService services.UserService) AuthController {
	return &authController{
		authServiceImpl: authServ,
		jwtService:      jwtServ,
		userServ:        userService,
	}
}

func (authController *authController) Login(ctx *gin.Context) {
	var loginDTO dto.LoginDTO
	errDTO := ctx.BindJSON(&loginDTO)
	if errDTO != nil {
		response := helper.ResponseRestuen(false, "invalid input", nil, errDTO.Error())
		ctx.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}
	if validationErr := validator.New().Struct(&loginDTO); validationErr != nil {
		fmt.Println(loginDTO)
		response := helper.ResponseRestuen(false, "bad request", nil, validationErr.Error())
		ctx.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}

	authResult, status := authController.authServiceImpl.Login(loginDTO)
	// fmt.Print(authResult)
	if authResult != nil && status {
		data := service.ServiceFormateUser(authResult)
		generateToken := authController.jwtService.GenerateToken(data.ID, loginDTO.Username)
		data.Token = generateToken
		authController.authServiceImpl.Logging(data)
		response := helper.ResponseRestuen(true, "go", map[string]interface{}{"data": data}, "")
		ctx.JSON(http.StatusOK, response)
		return
	}
	response := helper.ResponseRestuen(false, "invalid input", nil, "please check the correct username or password")
	ctx.AbortWithStatusJSON(http.StatusBadRequest, response)
}

func (authController *authController) LogOut(ctx *gin.Context) {
	authToken := ctx.GetHeader("Autherization")
	data, err := authController.userServ.GetUserByAtrribute("token", authToken)
	if err != nil {
		log.Fatalf("error to get user by this Atrribute %v\n ", err)
	}
	if data.Token == "" {
		response := helper.ResponseRestuen(false, "this not have token", nil, "error to get this token")
		ctx.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}
	data.Token = ""
	authController.authServiceImpl.Logging(data)
	response := helper.ResponseRestuen(true, "go", map[string]interface{}{"data": "now logout success"}, "")
	ctx.JSON(http.StatusOK, response)
}
