package controller

import (
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gitlab.com/ooatamelbugs/syntaxerror/dto"
	"gitlab.com/ooatamelbugs/syntaxerror/helper"
	"gitlab.com/ooatamelbugs/syntaxerror/models"
	"gitlab.com/ooatamelbugs/syntaxerror/services"
)

// interface for function that will impl
type UserController interface {
	CreateUser(ctx *gin.Context)
	UpdateUser(ctx *gin.Context)
	Getindex(ctx *gin.Context)
}

type userController struct {
	userService services.UserService
	urlService  services.UrlService
}

// NewUserController is a factory for init the user Controller
func NewUserController(userserv services.UserService, urlServ services.UrlService) UserController {
	return &userController{
		userService: userserv,
		urlService:  urlServ,
	}
}

// test function for test process of pipline data in app
func (usercontroller *userController) Getindex(ctx *gin.Context) {
	data, err := usercontroller.userService.IndexGet()
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, err)
	}
	response := helper.ResponseRestuen(true, "here data", map[string]interface{}{"data": data}, "")
	ctx.JSON(http.StatusOK, response)
}

// function for create new resource user
// and return the status code and data or error if exist
func (usercontroller *userController) CreateUser(ctx *gin.Context) {
	var user dto.UserDTO
	if err := ctx.BindJSON(&user); err != nil {
		response := helper.ResponseRestuen(false, "bad request", nil, err.Error())
		ctx.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}
	if validationErr := validator.New().Struct(&user); validationErr != nil {
		fmt.Println(user)
		response := helper.ResponseRestuen(false, "bad request", nil, validationErr.Error())
		ctx.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}
	result := usercontroller.userService.CreateUser(user)
	urllink, err := usercontroller.userService.ConfirmMail(user.Username)
	if err != nil {
		log.Fatalf("error in send mail %v", err)
	}
	// save data url
	var url = models.URL{
		UserID: result.ID,
		Url:    urllink,
		Expire: time.Now().Local().Add(time.Hour * time.Duration(72)),
		Secret: strings.Split(urllink, "active/")[1],
		Status: false,
	}
	usercontroller.urlService.AddURL(url)
	response := helper.ResponseRestuen(true, "success created new user", map[string]interface{}{"data": result}, "")
	ctx.JSON(http.StatusCreated, response)
}

// function for update resource user
// and return the status code and data or error if exist
func (usercontroller *userController) UpdateUser(ctx *gin.Context) {
	var user dto.UpdateUserDTO
	if err := ctx.BindJSON(&user); err != nil {
		response := helper.ResponseRestuen(false, "bad request", nil, err.Error())
		ctx.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}

	if validationErr := validator.New().Struct(&user); validationErr != nil {
		fmt.Println(user)
		response := helper.ResponseRestuen(false, "bad request", nil, validationErr.Error())
		ctx.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}
	userId := ctx.Param("id")
	result := usercontroller.userService.Update(userId, user)
	response := helper.ResponseRestuen(true, "ok update ", map[string]interface{}{"data": result}, "")
	ctx.JSON(http.StatusOK, response)
}
