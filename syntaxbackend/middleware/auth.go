package middleware

import (
	"log"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"gitlab.com/ooatamelbugs/syntaxerror/helper"
	"gitlab.com/ooatamelbugs/syntaxerror/utils/service"
)

func AutherizeJWT(jwtService service.JWTService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		authHeader := ctx.GetHeader("Autherization")
		if authHeader == "" {
			response := helper.ResponseRestuen(false, "fail in auth", nil, "no token")
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}
		token, err := jwtService.VerifyToken(authHeader)
		if token.Valid {
			claims := token.Claims.(jwt.MapClaims)
			log.Panicln("claims[user_id]: ", claims["user_id"])
		} else {
			log.Fatalf("err %v", err)
			response := helper.ResponseRestuen(false, "fail in auth", nil, err.Error())
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, response)
		}
	}
}
