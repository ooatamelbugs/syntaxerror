package repository

import (
	"gitlab.com/ooatamelbugs/syntaxerror/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type UrlRrepository interface {
	AddURL(url models.URL) (string, error)
	EditURL(url models.URL) bool
	GetURLBySecret(secret string) (models.URL, error)
}

type urlRrepository struct {
	collection *mongo.Collection
}

func NewUrlRrepository(collect *mongo.Collection) UrlRrepository {
	return &urlRrepository{
		collection: collect,
	}
}

func (urlRepo *urlRrepository) AddURL(url models.URL) (string, error) {
	newurl, err := urlRepo.collection.InsertOne(returnctx(), url)
	Obj, _ := newurl.InsertedID.(primitive.ObjectID)
	return Obj.Hex(), err
}

func (urlRepo *urlRrepository) EditURL(url models.URL) bool {
	var urlModel models.URL
	update := bson.M{
		"user_id": url.UserID,
		"url":     url.Url,
		"status":  url.Status,
		"expire":  url.Expire,
	}
	result := urlRepo.collection.FindOneAndUpdate(
		returnctx(),
		bson.M{"_id": url.ID},
		bson.M{"$set": update},
	)
	result.Decode(&urlModel)
	return result.Err() == nil
}

func (urlRepo *urlRrepository) GetURLBySecret(secret string) (models.URL, error) {
	var urlModel models.URL
	result := urlRepo.collection.FindOne(returnctx(), bson.M{"secret": secret})
	result.Decode(&urlModel)
	return urlModel, result.Err()
}
