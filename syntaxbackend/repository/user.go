package repository

import (
	"context"
	"time"

	"gitlab.com/ooatamelbugs/syntaxerror/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

// create UserRepository interface to make it guide to develop
type UserRepository interface {
	CreateUser(user models.User) (string, error)
	UpdateUser(user models.User) (models.User, error)
	DeleteUser(user models.User) (bool, error)
	GetUserByID(userID primitive.ObjectID) (bson.M, error)
	IndexGetString() (string, error)
	GetUserByAtrribute(field string, value interface{}) (models.User, error)
	VerifyUser(username string, password string) (bson.M, error)
}

type userRepositoryImpl struct {
	collection *mongo.Collection
}

func returnctx() context.Context {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	return ctx
}

func NewUserRepository(userCollection *mongo.Collection) UserRepository {
	return &userRepositoryImpl{
		collection: userCollection,
	}
}

func (userRepo *userRepositoryImpl) IndexGetString() (string, error) {
	text := "this is heare in user"
	return text, nil
}

func (userRepo *userRepositoryImpl) CreateUser(user models.User) (string, error) {
	newUser, err := userRepo.collection.InsertOne(returnctx(), user)
	userId, _ := newUser.InsertedID.(primitive.ObjectID)
	return userId.Hex(), err
}

func (userRepo *userRepositoryImpl) UpdateUser(user models.User) (models.User, error) {
	var userData models.User
	var idUser primitive.ObjectID = user.ID
	update := bson.M{
		"first_name": user.FirstName,
		"last_name":  user.LastName,
		"password":   user.Password,
		"active":     user.Active,
		"location":   user.Location,
		"token":      user.Token,
	}
	// fmt.Print(idUser)
	result := userRepo.collection.FindOneAndUpdate(
		returnctx(),
		bson.M{"_id": idUser},
		bson.M{"$set": update},
	)
	result.Decode(&userData)
	return userData, result.Err()
}

func (userRepo *userRepositoryImpl) DeleteUser(user models.User) (bool, error) {
	result := userRepo.collection.FindOneAndDelete(returnctx(), bson.M{"_id": user.ID})
	var dataDelete bson.M
	var status bool
	decodData := result.Decode(&dataDelete)
	if decodData != nil {
		status = false
	} else {
		status = true
	}
	return status, result.Err()
}

func (userRepo *userRepositoryImpl) GetUserByAtrribute(field string, value interface{}) (models.User, error) {
	var userData models.User
	err := userRepo.collection.FindOne(returnctx(), bson.D{primitive.E{Key: field, Value: value}}).Decode(&userData)
	return userData, err
}

func (userRepo *userRepositoryImpl) GetUserByID(userID primitive.ObjectID) (bson.M, error) {
	var user bson.M
	result := userRepo.collection.FindOne(returnctx(), bson.M{"_id": userID})
	result.Decode(&user)
	return user, result.Err()
}
func (userRepo *userRepositoryImpl) VerifyUser(username string, password string) (bson.M, error) {
	var user bson.M
	result := userRepo.collection.FindOne(returnctx(), bson.M{"username": username})
	result.Decode(&user)
	if result.Err() == nil {
		return user, nil
	}
	return nil, result.Err()
}
